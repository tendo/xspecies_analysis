#!/usr/bin/perl

use feature 'say';
use Digest::MD5 qw/md5/;

#my $pre='^(PREDICTED: (?:LOW QUALITY PROTEIN:|putative|protein |probable)?)?';
#my $suf='(-like(?:, partial| isoform X?\d+)?|isoform ?(?:[a-z]|X?\d+)?|homolog)$';

my @files = <*/protein/protein.fa.gz>;

open my $fh, "zcat @files | sed '1b;/^>/i\n'|" or die;
$/="";
while ($rec = <$fh>) {
    ($_, my $seq) = split /\n/, $rec, 2;
    chomp;
    s/^>gi.// or die "gi $?";  # first row
    s/\|(?:ref|gb)\|/\t/ or die "ref $_";
    s/\| ?/\t/ or die "> $_";
#    s/ \(Silurana\)//;  # fix irregular species naming
    s/ \[([ .A-Za-z]+?)]$/\t$1/ or die "sp $1; $_";

    $seq =~ s/\s//g;
    say $_, "\t", unpack('H*',md5($seq));
}
close my $fh;
