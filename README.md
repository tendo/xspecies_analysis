# README #

### What is this repository for? ###

This project is to build quick dirt summary for genome comparison by gene annotation in fasta files.
Fasta files will be automatically downloaded from NCBI repository.

### Prerequisites ###

* GNU Make
* Perl
* Perl module `Regexp::Assemble`

    `perl -MCPAN -e'install Regexp::Assemble'`

* A list of scientific names in question whose proteome data must be deposited in NCBI FTP site at `ftp://ftp.ncbi.nih.gov/genomes/`.

### How do I get set up? ###

* To obtain, type `git clone git@bitbucket.org:tendo/xspecies_analysis.git`.
* To build a list of scientific names, edit LIST text file.
* To execute, type `make`.

### Output files ###

* `sequence_header_txt` - a list of gene annotation.
* `spec_gene.txt` - a matrix for proteome comparison across species.
    [sapmle](http://cipro.ibio.jp/~tendo/bees_spec_genes.txt)


### `red.*`, `blue.*` text files  ###
They are just for us, sorry.
They used to be constructed based on plain text.
Now they are constructed as regular expression.

### Who do I talk to? ###

* `endo-xspa@ibio.jp`