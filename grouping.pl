#!/usr/bin/perl

use feature qw/say/;
use Regexp::Assemble;
my $ra = Regexp::Assemble->new;

my $namelist = shift;
my @namelist = `cat $namelist`;
chomp @namelist;

my $species = 'Apis mellifera|Eufriesea mexicana';

for my $name (@namelist) {
#    say STDERR $name;
    my @acc   = `fgrep "$name" @ARGV | egrep '$species' | cut -f2`;
    my @annot = `fgrep "$name" @ARGV | egrep '$species' | cut -f6`;
    chomp @acc;
    chomp @annot;
    $ra->add(map quotemeta, @annot);
#    say join "\t", $name, "@acc", "@annot";
    say join "\t", $name, "@acc", $ra->as_string;
}


