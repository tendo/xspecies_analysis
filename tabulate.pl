#!/usr/bin/perl -w
#

#1035585976      XP_016922127.1  PREDICTED: fasciclin-1 isoform X4       Apis cerana     5b5a68e160e87035975f7c713e28f9b7

use feature qw/say/;
use List::Util qw/sum/;

while (<>) {
    /uncharacterized protein/ and next;
    my @F=split /\t/;
    my $species = $F[3];
    ++$Nspec{$species};
    $F[2] =~ /^([A-Z]+: |putative|probable)* *(.*?) ?(LOC\d*)? ?(isoform X\d*|, partial)?$/;
    next unless $2;
    #my ($prefix,$gene,$suffix,$note)=($1,$2,$4,$3);
    #say join "\t", $species, $gene, $prefix?$prefix:'', $suffix?$suffix:'', $note?$note:'';
    $name = $2;
    $name =~ s/-like$//;
    $name =~ s/^LOW QUALITY PROTEIN: //;
    $name =~ s/^\s//;
    $name =~ s/\s+$//;
    ++$gene{$name}{$species};  # gene
}

my @SPEC = sort {$Nspec{$b} <=> $Nspec{$a}} keys %Nspec;

say join "\t", "name", "sum", @SPEC;

foreach my $name (keys %gene) {
    my @val = map{$gene{$name}{$_} or 0} @SPEC;
    say join "\t", $name, sum(@val), @val;
}
