#!/usr/bin/perl -w
# stemmed.pl converts verbose annotation into concise stem description.
# the annotation was reformed from FASTA header to TAB separated.
#
# Toshinori Endo 2017.1.12

# input: TAB-separated; annotation in the third column
# 1035585976      XP_016922127.1  PREDICTED: fasciclin-1 isoform X4       Apis cerana     5b5a68e160e87035975f7c713e28f9b7
#
# output: TAB-separated: gi accession name species hash annotation 


use feature qw/say/;

while (<>) {
    /uncharacterized protein/ and next;
    chomp;
    my ($gi, $acc, $annot, $species, $hash) = split /\t/;

    # filter meaningless terms
    $annot =~ /^([A-Z]+: |putative|probable)* *(.*?) ?(LOC\d+)? ?(isoform X\d*|, partial|precursor)?$/;
    $2 or next;   # skip if no info
    #my ($prefix,$gene,$suffix,$note)=($1,$2,$4,$3);
    #say join "\t", $species, $gene, $prefix?$prefix:'', $suffix?$suffix:'', $note?$note:'';

    $name = $2;
    $name =~ s/^LOW QUALITY PROTEIN: //;
    $name =~ s/^\s+//;    # remove leading space

    $name =~ s/-like( \d+)?$//; 
    $name =~ s/ [A-Z]*\d+$//;
    $name =~ s/\s+$//;    # remove trailing space

    say join "\t", $gi, $acc, $name, $species, $hash, $annot;
}
