PATH := .:$(PATH)

TARGETS := spec_gene.txt red.idlist blue.idlist red.diff.txt blue.diff.txt
PERL_MODULES := Regexp\:\:Assemble
PREP := check_perl_modules

all: $(PREP) $(TARGETS)

%.diff.txt: %.list
	sort $*.list>$*.list1
	sort ../bees/$*.list>$*.list2
	diff -u $*.list1 $*.list2 >$@ || /bin/true

%.orphan.txt: %.orphan
	grep -Ff $< sequence_header_n.txt >$@

%.orphan: %.idlist
	awk -F'\t' '$$2=="" {print $$1}' $< >$@

# idlist groups
%.idlist: %.list sequence_header_n.txt
	grep -Ff $^ | sort +2 | egrep 'Eufriesea|Apis mellifera' > $<.tmp
	grouping.pl $< $<.tmp >$@

red.list: spec_gene.txt
	awk -F'\t' '$$3>0 && $$10==0 {print $$1}' $< >$@

blue.list: spec_gene.txt
	awk -F'\t' '$$3==0 && $$10>0 {print $$1}' $< >$@


# species to gene presence matrix
spec_gene.txt: tabulate.pl sequence_header_n.txt
	$^ | uniq | sort -t $$'\t' -k3n -k10n -k2r >$@

# stemmed sequnece header lines (gene names stemmed)
sequence_header_n.txt: stemmed.pl sequence_header.txt
	$^ >$@

# sequence header lines
sequence_header.txt: get_sequence_header.pl fetch
	$< >$@

# fetch
ncbi_repo := ftp://ftp.ncbi.nih.gov/genomes/
WGET := wget -nv -c -m -nH --cut-dirs=1
fetch: LIST
	sed 's/ /_/g;s,^,$(ncbi_repo),;s,$$,/protein/protein.fa.gz,' $< | $(WGET) -i -

# clean
gitclean: ; git clean -df

.PHONY: check_perl_modules
check_perl_modules:
	echo $(PERL_MODULES)
	@for pm in $(PERL_MODULES); do \
	perl -M"$$pm" -e' ' >/dev/null || perl -MCPAN -e"install $$pm"; \
	done

